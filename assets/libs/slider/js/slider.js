(function ($) {
    $.extend({
        initSlide: function () {
            $('.slide').eq(0).show().addClass('active');
        },
        moveSlide: function (direction) {
            var max_slide = $('.slide').length;
            var index = $('.slide.active').index() + direction;
            index = index > max_slide-1 ? 0 : index;
            index = index < 0 ? max_slide-1 : index;
            $('.slide').fadeOut('slow','swing', function(){
            }).removeClass('active').addClass('inactive');
            $('.slide').eq(index).fadeIn('slow','swing').addClass('active').removeClass('inactive');
        }
    });
})(jQuery);

$(document).ready(function(){
    console.log('loaded');
    var max_slide = $('.slide').length;
    if (max_slide>0) {
        $.initSlide();

        $('.navi').click(function(){
            $.moveSlide(parseInt($(this).data('direction')));
        });
    }
});